package pl.nicramus.mirko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
	
	/**
	 * Used when run as a jar
	 * @param args
	 */

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
//@EnableJpaRepositories
@EnableDiscoveryClient
public class MirkoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MirkoApplication.class, args);
	}
	
	
//	/**
//	 * Used when run as a war
//	 */
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
//		return builder.sources(MirkoApplication.class);
//	}

}
