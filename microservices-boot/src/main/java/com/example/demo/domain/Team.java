package com.example.demo.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Team {
	@Id @GeneratedValue
	private Long id;
	private String name;
	private String location;
	private String mascotte;
	@OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="teamId")
	private Set<Player> players;
	
	@XmlElement  
	public Set<Player> getPlayers() {
		return players;
	}

	public Team() {
		super();
	}
	
	public Team(String location, String name, Set<Player> players) {
		this();
		this.location = location;
		this.name = name;
		this.players = players;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	
//	public void setLocation(String location) {
//		this.location = location;
//	}
//	
	public String getMascotte() {
		return mascotte;
	}
	
//	public void setMascotte(String mascotte) {
//		this.mascotte = mascotte;
//	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	
}
