package com.example.demo.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.example.demo.domain.Team;

//@RestResource(path="teams", rel="teams")
@RepositoryRestResource(path="teams", itemResourceRel="teams")
public interface TeamDao extends JpaRepository<Team, Long> {
	List<Team> findAll();
	Team findByName(String name);
}
