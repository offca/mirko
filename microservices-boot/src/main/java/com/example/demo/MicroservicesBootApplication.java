package com.example.demo;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.Persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import com.example.demo.dao.TeamDao;
import com.example.demo.domain.Player;
import com.example.demo.domain.Team;

@SpringBootApplication
public class MicroservicesBootApplication {
	
	@Autowired
	TeamDao teamDao;
	
	@PostConstruct
	public void init() {
		Set<Player> players = new HashSet<>();
		players.add(new Player("Jurek", "pitcher"));
		players.add(new Player("Tomasz", "shhortstop"));
		
		Team team = new Team("Tuchola", "crapy", players);
		teamDao.save(team);
	}
	
//	@PreDestroy
//	public void cleanUp() throws Exception {
//	  System.out.println("Spring Container is destroy! Customer clean up");
//	  org.hsqldb.DatabaseManager.closeDatabases(0);
//	}

	public static void main(String[] args) {
		SpringApplication.run(MicroservicesBootApplication.class, args);
		//Persistence.generateSchema("samplePU", null);
	}
}
