package com.example.demo;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.TeamDao;
import com.example.demo.domain.Player;
import com.example.demo.domain.Team;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import groovy.util.logging.Slf4j;

@Slf4j
@RepositoryRestController
public class HelloPageController {
	
	private Logger logger = LoggerFactory.getLogger(HelloPageController.class);
	
	@Autowired
	private TeamDao teamDao;
	
//	@RequestMapping("/ @ResponseBodyi/{name}")
//	public String hello(Map<String, String> model, @PathVariable String name) {
//		model.put("name", name);
//		return "hello";
//	}
////	
//	@RequestMapping("/elo/{name}")
//	public Team teamResponse(@PathVariable String name) {
//		return teamDao.findByName(name);
//	}
//	
//	@RequestMapping("/elo/elo")
//	public String strResponse() {
//		return "response";
//	}
	
//	public HelloPageController(PagedRes) {
//		
//	}
//	
	@RequestMapping(value = "/teams/{id}", method = RequestMethod.PUT)
	@ResponseBody //@RequestBody Team team, 
	public ResponseEntity<Team> overridedPutResponse(@PathVariable Long id, @RequestBody Team teamParameter) 
			throws JsonProcessingException, IOException {		
		teamParameter.setId(id);
		Team save = teamDao.save(teamParameter);
		logger.info(save.toString());
		return new ResponseEntity<>(save, HttpStatus.NO_CONTENT);
	}
	
	
}
