package com.example.demo.configuration;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.http.converter.HttpMessageConverter;

@Configuration
public class CustomConfiguration {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	 @Bean
	  public RepositoryRestConfigurer repositoryRestConfigurer() {

	    return new RepositoryRestConfigurerAdapter() {

//	      @Override
//	      public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
//	        //config.setBasePath("/api");
//	      }
	    	
	    	@Override
	    	public void configureHttpMessageConverters(List<HttpMessageConverter<?>> messageConverter) {
	    		log.debug("configure message converters");
	    		
	    	}
	    
	    
	    };
	    
	    
	  }
	
}
