package com.example.demo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestMyController {
	
	private MockMvc mockMvc;	
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(new MyController()).build();		
	}
	
	@Test
	public void testHello() throws Exception {
		this.mockMvc.perform(get("/").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
			.andExpect(status().isOk());
//			.andExpect(content().contentType("application/json"));
	}
	
	
	@Test
	public void testHello2() throws Exception {
		this.mockMvc.perform(get("/asdf").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
			.andExpect(status().isOk());
//			.andExpect(content().contentType("application/json"));
	}

}

