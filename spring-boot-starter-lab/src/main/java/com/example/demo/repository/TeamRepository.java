package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.example.demo.model.Team;



@Repository
//todo custom responses 
public interface TeamRepository extends CrudRepository<Team, Long> {
	
 }

