package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.example.demo.model.Player;

@RepositoryRestResource(path="players", itemResourceRel="player")
public interface PlayerRepository extends CrudRepository<Player, Long>{

}
