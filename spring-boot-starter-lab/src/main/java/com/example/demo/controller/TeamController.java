package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.model.Coords;
import com.example.demo.model.Location;
import com.example.demo.model.Player;
import com.example.demo.model.Something;
import com.example.demo.model.Team;
import com.example.demo.repository.TeamRepository;

@RestController
public class TeamController {
	
	private final List<Team> list = new ArrayList<>();	
	
	@Autowired
	private TeamRepository teamRepository;
	
	
	@PostConstruct
	public void init() {
		Coords coords = new Coords(53.1943d, 16.1715);
     	Location location = new Location("Koszalin", coords);
     	
     	Set<Player> set1 = new HashSet<>();     	
     	set1.add(new Player("Big Easy", "Showman"));
      	set1.add(new Player("Buckets", "Guard"));
      	set1.add(new Player("Dizzy", "Guard"));
     	Team team = new Team(1l, "Globalnelogiki", location, set1);
		
     	Set<Player> set2 = new HashSet<>();     	
     	set2.add(new Player("Big Easy", "Showman"));
      	set2.add(new Player("Buckets", "Guard"));
      	set2.add(new Player("Dizzy", "Guard"));
		
		Team team2 = new Team(1l, "Raporciaki", location, set2);
		teamRepository.save(team);
		teamRepository.save(team2);	
	}
	
	@RequestMapping("/teams")
	public Iterable<Team> getTeams() {		
		return teamRepository.findAll();
	}
	
	@RequestMapping("/team/{id}")
	public Team getTeam(@PathVariable Long id) {
		return teamRepository.findOne(id);
	}
	
}
