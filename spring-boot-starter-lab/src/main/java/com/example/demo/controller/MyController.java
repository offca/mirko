package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyController {
	
	@RequestMapping("/")
	public String hello() {
		return "hello";
	}
	
	
	@RequestMapping("/asdf")
	public String hello2() {
		return "hello";
	}
}
