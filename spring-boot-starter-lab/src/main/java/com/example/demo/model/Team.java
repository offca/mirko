package com.example.demo.model;

import java.beans.Transient;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Team {
	@Id @GeneratedValue
	private Long id;
	private String name;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="teamId")
	private Location location;
	
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinColumn(name="teamId")
	private Set<Player> players;
	
	protected Team() {
		
	}
	
	public Team(Long id, String name, Location location, Set<Player> players) {
		this.id = id;
		System.out.println("gtggg");
		this.name = name;
		this.location = location;
		this.players = players;
	}
	
//	public Team(Long id, String name, Location location) {
//		this.id = id;
//		this.name = name;
//		this.location = location;
//	}
	
	public Long getId() {
		return id;
	}
	
	
	public String getName() {
		return name;
	}

	public Location getSomething() {
		return location;
	}
	
	protected void setName(String name) {
		this.name = name;
	}

	protected void setSomething(Location location) {
		this.location = location;
	}


}
