package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Coords {
	@Id @GeneratedValue
	private Long id;
	private double latitude;
	private double longitude;	
	
	protected Coords() {
		
	}

	public Coords(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}	
	
	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}
	
	protected void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	protected void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
}
