package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Player {
	
	@Id @GeneratedValue
	private Long id;
	private String name;
	private String position;
	
	public Player(String name, String position) {
		this.name = name;
		this.position = position;
	}
	
	public Player() {
		
	}
	
	public String getName() {
		return name;
	}
	
	protected void setName(String name) {
		this.name = name;
	}
	
	public String getPosition() {
		return position;
	}
	
	protected void setPosition(String position) {
		this.position = position;
	}
}
