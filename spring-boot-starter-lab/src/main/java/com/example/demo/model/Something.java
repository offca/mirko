package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Something {
	@Id @GeneratedValue
	private Long id;
	private String e;
	
	public Something() {
		
	}
	
	public String getE() {
		return e;
	}

	protected void setE(String e) {
		this.e = e;
	}
}
