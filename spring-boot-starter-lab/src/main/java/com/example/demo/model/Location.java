package com.example.demo.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Location {
	@Id @GeneratedValue
	private Long id;
	private String locationName;
	@JoinColumn(name="idCoords")
	@OneToOne(cascade= {CascadeType.ALL})
	private Coords coordinates;
	
	protected Location() {
		
	}
	
	public Location(String locationName, Coords coordinates) {
		this.locationName = locationName;
		this.coordinates = coordinates;
	}	
	
	public String getLocationName() {
		return locationName;
	}

	public Coords getCoordinates() {
		return coordinates;
	}
	
	protected void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	protected void setCoordinates(Coords coordinates) {
		this.coordinates = coordinates;
	}
}
