package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	@Autowired
	//@Lazy
	private DiscoveryClient discoveryClient;
	
	@RequestMapping("/helo") 
	public List<String> getListStrings() {
		List<String> list = new ArrayList<>();
		list.add("abra");
		return discoveryClient.getServices();
	}
	
	@RequestMapping("/number") 
	public int lllll() {
		return 0;
	}
}
