package com.example.demo;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
//import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.EnableZuulServer;
import org.springframework.context.annotation.Lazy;

//import com.netflix.discovery.DiscoveryClient;

@SpringBootApplication
//@EnableZuulServer
@EnableEurekaClient
@EnableZuulProxy
public class LabGatewayApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(LabGatewayApplication.class);	
	
	//@Autowired
	//private static DiscoveryClient discoveryClient;
	

	
	public static void main(String[] args) {
		SpringApplication.run(LabGatewayApplication.class, args);
		logger.info("APPLICATION STARTED");
		
		//postInit();
	
	
//		
//		if (services != null) {
//			logger.info("services not null");
//		} else{
//			logger.info("services null");
//		}
//		
//		 for (String service : services) {
//			
//		 }
		 
	}
	
//	@PostConstruct
//	private static void postInit() {
//		List<String> services = discoveryClient.getServices();
//	}
}
