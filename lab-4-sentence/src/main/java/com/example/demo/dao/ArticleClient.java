package com.example.demo.dao;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.dao.domain.Word;

@FeignClient(name="LAB-4-ARTICLE")
public interface ArticleClient {
	@RequestMapping(method = RequestMethod.GET, value = "/")
	public Word getArticle();
}
