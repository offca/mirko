package com.example.demo.dao;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.dao.domain.Word;

@FeignClient(name="LAB-4-ADJECTIVE")
public interface AdjectiveClient {
	@RequestMapping(method = RequestMethod.GET, value = "/")
	public Word getAdjective();
}
