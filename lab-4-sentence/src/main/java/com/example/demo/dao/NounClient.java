package com.example.demo.dao;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.dao.domain.Word;

@FeignClient(name="LAB-4-NOUN")
public interface NounClient {	
	@RequestMapping(method = RequestMethod.GET, value = "/")
	public Word getNoun();
}
