package com.example.demo.service;

import com.example.dao.domain.Word;

public interface WordService {
	public Word getNoun();
	public Word getAdjective();
	public Word getArticle();
	public Word getSubject();
	public Word getVerb();
}
