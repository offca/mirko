package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.domain.Word;
import com.example.demo.dao.AdjectiveClient;
import com.example.demo.dao.ArticleClient;
import com.example.demo.dao.NounClient;
import com.example.demo.dao.SubjectClient;
import com.example.demo.dao.VerbClient;

@Service
public class SentenceServiceImpl implements SentenceService {
	
	@Autowired
	private final WordService wordService;
		
	@Autowired
	public SentenceServiceImpl(WordService wordService) {
		this.wordService = wordService;
	}
	
	
	@Override
	public String yodaService() {		
		String nounStr = wordService.getNoun().getWord();
		String adjectiveStr = wordService.getAdjective().getWord();
		String articleStr = wordService.getArticle().getWord();
		String subjectStr = wordService.getSubject().getWord();
		String verbStr = wordService.getVerb().getWord();
		
		String sentence = String.format("%s %s %s %s %s.", nounStr, 
										adjectiveStr, 
										articleStr, 
										subjectStr, 
										verbStr);		
			
		return sentence;
	}


	@Override
	public String buildService() {
		String nounStr = wordService.getNoun().getWord();
		String adjectiveStr = wordService.getAdjective().getWord();
		String articleStr = wordService.getArticle().getWord();
		String subjectStr = wordService.getSubject().getWord();
		String verbStr = wordService.getVerb().getWord();
		
		String sentence = String.format("%s %s %s %s %s.", subjectStr, 
										verbStr, 
										articleStr, 
										nounStr, 
										adjectiveStr);		
			
		return sentence;
	}

}
