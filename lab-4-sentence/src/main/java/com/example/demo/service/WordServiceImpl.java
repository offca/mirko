package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.domain.Word;
import com.example.demo.dao.AdjectiveClient;
import com.example.demo.dao.ArticleClient;
import com.example.demo.dao.NounClient;
import com.example.demo.dao.SubjectClient;
import com.example.demo.dao.VerbClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class WordServiceImpl implements WordService {
	
	private NounClient nounClient;	
	private AdjectiveClient adjectiveClient;
	private ArticleClient articleClient;
	private SubjectClient subjectClient;
	private VerbClient verbClient;
	
	
	@Autowired
	public void setNounClient(NounClient nounClient) {
		this.nounClient = nounClient;
	}
	
	@Autowired
	public void setAdjectiveClient(AdjectiveClient adjectiveClient) {
		this.adjectiveClient = adjectiveClient;
	}	

	@Autowired
	public void setAdjectiveClient(ArticleClient articleClient) {
		this.articleClient = articleClient;
	}
	
	@Autowired
	public void setSubjectClient(SubjectClient subjectClient) {
		this.subjectClient = subjectClient;
	}	
	
	@Autowired
	public void setVerbClient(VerbClient verbClient) {
		this.verbClient = verbClient;
	}		
	
	@Override
	@HystrixCommand(fallbackMethod="getFallbackNoun")
	public Word getNoun() {
		return nounClient.getNoun();
	}

	@Override
	@HystrixCommand(fallbackMethod="getFallbackAdjective")
	public Word getAdjective() {		
		return adjectiveClient.getAdjective();
	}

	@Override
	public Word getArticle() {		
		return articleClient.getArticle();
	}

	@Override
	@HystrixCommand(fallbackMethod="getFallbackSubject")
	public Word getSubject() {		
		return subjectClient.getSubject();
	}

	@Override
	public Word getVerb() {		
		return verbClient.getVerb();
	}
		
	public Word getFallbackNoun() {
		return new Word("something noun");
	}
	
	public Word getFallbackSubject() {
		return new Word("Someone");
	}
	
	public Word getFallbackAdjective() {
		return new Word("");
	}
	

}
