package com.example.demo.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EurekaDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dao.NounClient;
import com.example.demo.service.SentenceService;


@RefreshScope
@RestController
public class SentenceController {
	private final SentenceService sentenceService;
		
	@Autowired
	public SentenceController(SentenceService sentenceService) {	
		this.sentenceService = sentenceService;
	}
	
	
	@RequestMapping("/sentence-yoda")
	public @ResponseBody String getSentenceOld() {
		String result = sentenceService.yodaService();
		return result;
	}
	
//	private String getWord(String service) {		
//		ServiceInstance servInstance = loadBalancedClient.choose(service);
//		if (servInstance != null ) {
//			URI uri = servInstance.getUri();
//			if(uri != null) {
//				return (new RestTemplate()).getForObject(uri, String.class);
//			}
//		}
//		return null;
//	}
	
	@RequestMapping("/sentence")
	public @ResponseBody String getSentence() {
		return sentenceService.buildService();
	}
	
}
