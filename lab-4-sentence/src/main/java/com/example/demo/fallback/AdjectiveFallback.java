package com.example.demo.fallback;

import org.springframework.stereotype.Component;

import com.example.dao.domain.Word;
import com.example.demo.dao.AdjectiveClient;

//@Component
public class AdjectiveFallback implements AdjectiveClient {

	@Override
	public Word getAdjective() {		
		return new Word("");
	}
	
}
