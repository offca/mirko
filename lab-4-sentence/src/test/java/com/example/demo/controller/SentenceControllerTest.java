package com.example.demo.controller;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.demo.service.SentenceService;

@RunWith(SpringJUnit4ClassRunner.class)
public class SentenceControllerTest {
	
	private MockMvc mockMvc;
	
	@Mock
	private SentenceService sentenceService;
	
	@Before
	public void setup() {
		//MockMvcBuilders.standaloneSetup(controllers)
		this.mockMvc = MockMvcBuilders.standaloneSetup(new SentenceController(sentenceService)).build();
	}
	
	@Test
	public void testSentenceControllerResponse() throws Exception {
		this.mockMvc.perform(get("/sentence"))
			.andExpect(status().isOk());
			//.andExpect(content().contentType(MediaType.ALL));
	}
}
