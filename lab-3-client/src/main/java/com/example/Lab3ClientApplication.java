package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.PropertySource;

//import com.example.config.ConfigBinder;


@SpringBootApplication
@EnableConfigurationProperties
//@PropertySource("lab-3-client.yml")
public class Lab3ClientApplication {
	private final static Logger logger = LoggerFactory.getLogger(Lab3ClientApplication.class);
	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(Lab3ClientApplication.class, args);
		//final ConfigBinder conf = ctx.getBean(ConfigBinder.class);
		
		String aaa = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
		logger.debug(aaa);
		logger.error(aaa);
		//logger.info(conf.getPreamble());
		logger.info(aaa);
		logger.warn(aaa);
		
	}
}
