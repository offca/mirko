package com.example.demo.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.config.GlobalProperties;



/***
 * We use refresh scoper when we got stateful bean - 
 * if we store state here @RefreshScope is mannatory if we want to get the newest value 
 * @author marcin.mielcarski
 *
 */
@RefreshScope
@RestController
public class LuckyWordController {
	//TODO zwalidowac jakos te configuratiojn properties.....
	//@Value("${lucky-word}") 
	//@Autowired
	@Autowired GlobalProperties globalProperties;
	
	String fullStatement;

	@RequestMapping("/lucky-word")
	public String showLuckyWord() {
		return fullStatement;
	}	
	
	@PostConstruct	
	public String init() {
		fullStatement = globalProperties.getEmail() + " " + globalProperties.getThreadPool();
		return fullStatement;
		
	}
	
	
}	

